# ipbot4weixin

#### 介绍
通常，宽带运营商只能提供动态IP地址。所以，一般公司和家庭互联网的出口IP是随机不定时变化的。
某些场景下，在办公室或家里访问外部服务，需要提供自己的出口IP。服务方会将该IP写入白名单中，只接受来自白名单中的IP访问请求。
当IP发生变化之后，需要重新提供新的IP。如此循环往复。

本服务是一个shell脚本，可获取当前的外网IP，并通过企业微信机器人，发布IP信息到相关的群中。
服务会自动检测外网IP，并和上次的IP进行比较，只有IP变化之后才会发送消息。


#### 安装与运行
服务可运行在Linux、Mac以及部分路由器中。
建议通过定时任务的方式，定期执行。
示例：每天的8-23点，每隔5分钟执行一次
```
*/5 8-23 * * * /jffs/scripts/ipbot4weixin.sh
```

#### 参数
```
fyBotApiUrl="https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=xxxxxxxx-4206-42d9-94bd-xxxxxxxxxxxx"
fyIpFile="/Users/yi/ipbot4weixin-Ip.log"
fyLogFile="/Users/yi/ipbot4weixin-Run.log"
```
fyBotApiUrl： 在企业微信中申请的机器人的webhook url。 注意，勿外泄！
fyIpFile: 用于记录上一次IP的文件
fyLogFile: 用于记录最后一次的运行日志

