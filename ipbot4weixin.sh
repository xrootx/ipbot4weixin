#!/bin/sh
#

# Get WAN IPv4
fyGetWanIp4() {

    local hostIp

    hostIp=$(curl -s https://v4.myip.la)

    if [ -z "$hostIp" ]; then
        echo "fyGetWanIp4 - Can't get ip address"
        return 1
    fi

    if [ -z "$(echo $hostIp | grep -E '^[0-9\.]+$')" ]; then
        echo "fyGetWanIp4 - Invalid ip address $hostIp"
        return 1
    fi

    echo $hostIp

}

# Get last ip saved in local file
fyGetLogIp() {

    local errMsg
    local logIp

    if [ ! -f $fyIpFile ]; then 
	    touch $fyIpFile 
        echo "0.0.0.0" > $fyIpFile
    fi

    if [ ! -f $fyIpFile ]; then 
        echo "fyGetIpFile - Invalid ip file $fyIpFile"
	    return 1 
    fi

    logIp=$(cat $fyIpFile)

    if [ -z "$(echo $logIp | grep -E '^[0-9\.]+$')" ]; then
        echo "fyGetIpLog - Invalid log ip address $logIp"
        return 1
    fi

    logIp=$(echo $logIp | sed 's/.*,"value":"\([0-9a-fA-F\.\:]*\)".*/\1/')

    case "$logIp" in
        [0-9]*)
            echo $logIp
            return 0
        ;;
        *)
            errMsg=$(echo $logIp | sed 's/.*"message":"\([^\"]*\)".*/\1/')
            echo "fyGetLogIp - $errMsg"
            return 1
        ;;
    esac

}


fyIpChangeNotify() {

    local recordRs

    local errCode
    local errMsg

    if [ -z "$1" ]; then
        echo "fyIpChangeNotify - Args number error"
        return 1
    fi

    recordRs=$(curl -sS $fyBotApiUrl \
                 -H "Content-Type: application/json" \
                 -d "{\"msgtype\": \"text\", \"text\": {\"content\": \"我是路由器，来自：$1\" } } ")

    echo "> Notify by Bot: $recordRs" >> $fyLogFile
    
    errCode=$(echo $recordRs | sed 's/.*"errcode":\([0-9]*\).*/\1/')
    errMsg=$(echo $recordRs | sed 's/.*,"errmsg":"\([^"]*\)".*/\1/')
    
    if [ $errCode -ne 0 ]; then
        echo "IpChangeNotify - $recordRs"
        return 1
    else
        echo "IpChangeNotify - success"
        return 0
    fi

}


# Main Sub
fyIpCheck() {

    local errCode

    local hostIp
    local lastIp
    local postRs

    echo "  Fetching Host Ip"
    hostIp=$(fyGetWanIp4)

    errCode=$?
    echo "> Host Ip: $hostIp"
    echo "> Host Ip: $hostIp" > $fyLogFile
    if [ $errCode -ne 0 ]; then
        return 1
    fi

    echo "  Checking Last IP Record"
    lastIp=$(fyGetLogIp)

    errCode=$?
    echo "> Last Ip: $lastIp"
    echo "> Last Ip: $lastIp" >> $fyLogFile

    if [ $errCode -ne 0 ]; then
        return 1
    fi

    if [ "$lastIp" = "$hostIp" ]; then
        echo "> Last Ip is the same as host Ip. Bye!"
        return 0
    fi

    echo "  IP changed"
    postRs=$(fyIpChangeNotify "$hostIp")

    errCode=$?
    echo "> $postRs"
    if [ $errCode -ne 0 ]; then
        echo "  error, exit !"
        return 1
    fi

    echo $hostIp > $fyIpFile
    echo "  OK, finished !"
}

fyBotApiUrl="https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=1afc3d116132-4206-42d9-94bd-cfd5cb5bd18b"
fyIpFile="/Users/yi/Downloads/fyBotLastIp.log"
fyLogFile="/Users/yi/Downloads/fyBotLastRun.log"
#fyIpFile="/jffs/scripts/ip-bot/lastip.log"
#fyLogFile="jffs/scripts/ip-bot/debug.log"

fyIpCheck